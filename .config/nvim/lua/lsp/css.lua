local capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())
local lsp_config = require("lspconfig")
local on_attach = require("lsp/on_attach")

-- highlight word under cursor occurences
local function lsp_highlight_document(client)
	if client.server_capabilities.document_highlight then
		vim.api.nvim_exec(
			[[
            augroup lsp_document_highlight
                autocmd! * <buffer>
                autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
                autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
            augroup END
            ]],
			false
		)
	end
end

lsp_config.cssls.setup({
	capabilities = capabilities,
	filetypes = { "css", "sass", "scss" },
	settings = {
		css = {
			validate = true,
			unknownAtRules = "ignore",
		},
		sass = {
			validate = true,
		},
		scss = {
			validate = true,
		},
	},
	on_attach = function(client)
		client.server_capabilities.documentFormattingProvider = false
		on_attach(client)
		lsp_highlight_document(client)
	end,
})
