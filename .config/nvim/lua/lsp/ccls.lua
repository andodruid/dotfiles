local util = require("lspconfig.util")
local lspconfig = require("lspconfig")
local on_attach = require("lsp/on_attach")
local capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())

local function lsp_highlight_document(client)
	if client.server_capabilities.document_highlight then
		vim.api.nvim_exec(
			[[
            augroup lsp_document_highlight
                autocmd! * <buffer>
                autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
                autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
            augroup END
            ]],
			false
		)
	end
end

lspconfig.ccls.setup({
	capabilities = capabilities,
	on_attach = function(client)
		client.server_capabilities.documentFormattingProvider = false
		on_attach(client)
		lsp_highlight_document(client)
	end,
	default_config = {
		cmd = { "ccls" },
		filetypes = { "c", "cpp" },
		root_dir = util.root_pattern("compile_commands.json", ".ccls", ".git"),
		offset_encodings = { "utf-8" },
		-- ccls does not support sending a null root directory
		single_file_support = false,
	},
	init_options = {
		compilationDatabaseDirectory = "build",
		index = {
			threads = 0,
		},
		clang = {
			excludeArgs = { "-frounding-math" },
		},
	},
})

local notify = vim.notify
vim.notify = function(msg, ...)
	if msg:match("warning: multiple different client offset_encodings") then
		return
	end

	notify(msg, ...)
end
