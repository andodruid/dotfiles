local lsp_config = require("lspconfig")
local on_attach = require("lsp/on_attach")
local capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())

lsp_config.pyright.setup({
	capabilities = capabilities,
	on_attach = function(client)
		client.server_capabilities.documentFormattingProvider = false
		on_attach(client)
	end,
})
