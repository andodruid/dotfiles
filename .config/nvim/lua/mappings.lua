local map = vim.api.nvim_set_keymap

map("n", "<c-s>", ":w<CR>", { noremap = true })
map("i", "<c-s>", "<Esc>:w<CR>a", { noremap = true })

--floaterm
map("n", "<C-g>i", ":FloatermNew --height=0.9 --width=0.9 --wintype=float lazygit<CR>", { noremap = true })
map("n", "<C-g>t", ":FloatermNew --height=0.9 --width=0.9 --wintype=float <CR>", { noremap = true })
map("n", "<C-g>n", ":FloatermNext<CR>", { noremap = true })
map("n", "<C-g>p", ":FloatermPrev<CR>", { noremap = true })
map("n", "<C-g>o", ":FloatermShow<CR>", { noremap = true })
map("n", "<C-g>q", "<C-\\><C-n>:FloatermKill<CR>", { noremap = true })
map("t", "<C-g>o", "<C-\\><C-n>:FloatermToggle<CR>", { noremap = true })

-- :vertical resize w<CRw<CR>>+5
-- :vertical resize -5
-- resize windows
map("n", "<C-w><", ":vertical resize -5<CR>", { noremap = true })
map("n", "<C-w>>", ":vertical resize +5<CR>", { noremap = true })
map("n", "<C-w>+", ":resize +5<CR>", { noremap = true })
map("n", "<C-w>_", ":resize -5<CR>", { noremap = true })

-- buffer navigation
map("n", "<Leader>,", "<C-^>", { noremap = true }) -- access last open buffer
map("n", "<Leader>bl", ":buffers<CR>", { noremap = true })
map("n", "<S-l>", ":bnext<CR>", { noremap = true })
map("n", "<S-h>", ":bprev<CR>", { noremap = true })

-- map paste buffer
map("n", "<Leader>p", '"0p', { noremap = true }) -- access last open buffer
map("v", "<Leader>p", '"0p', { noremap = true }) -- access last open buffer

-- go normal mode
map("i", "jj", "<Esc>", { noremap = true })

-- keep visual selection when indenting
map("v", "<", "<gv", { noremap = true, silent = true })
map("v", ">", ">gv", { noremap = true, silent = true })

-- search and replace the word under cursor
map("n", "<Leader>*", ":%s/<C-r><C-w>//<Left>", { noremap = true })

-- quit faster
map("n", "q<Tab>", ":q<CR>", { noremap = true })

-- to move between wrapped lines
map("n", "j", "gj", { noremap = true })
map("n", "k", "gk", { noremap = true })
map("n", "gj", "j", { noremap = true })
map("n", "gk", "k", { noremap = true })

-- quickfixlist
map("n", "<Leader>qn", ":cnext<CR>zz", { noremap = true })
map("n", "<Leader>qp", ":cprevious<CR>zz", { noremap = true })
-- map("n", "<Leader>ql", ":copen<CR>", { noremap = true })

-- delete current buffer
map("n", "<Leader>db", ":bd<CR>", { noremap = true })

-- map going up a full window height and down but keeping cursor centered
map("n", "<C-d>", "<C-d>zz", { noremap = true })
map("n", "<C-u>", "<C-u>zz", { noremap = true })

-- center search highlighted terms on screen
map("n", "N", "Nzz", { noremap = true })
map("n", "n", "nzz", { noremap = true })

-- paste buffer 0
map("n", "<Leader>pb", '"0p"', { noremap = true })

-- nvim-dap
map("n", "<leader>dct", '<cmd>lua require"dap".continue()<CR>', { noremap = true })
map("n", "<leader>dsv", '<cmd>lua require"dap".step_over()<CR>', { noremap = true })
map("n", "<leader>dsi", '<cmd>lua require"dap".step_into()<CR>', { noremap = true })
map("n", "<leader>dso", '<cmd>lua require"dap".step_out()<CR>', { noremap = true })
map("n", "<leader>dtb", '<cmd>lua require"dap".toggle_breakpoint()<CR>', { noremap = true })

map(
	"n",
	"<leader>dsbr",
	'<cmd>lua require"dap".set_breakpoint(vim.fn.input("Breakpoint condition: "))<CR>',
	{ noremap = true }
)
map(
	"n",
	"<leader>dsbm",
	'<cmd>lua require"dap".set_breakpoint(nil, nil, vim.fn.input("Log point message: "))<CR>',
	{ noremap = true }
)
map("n", "<leader>dro", '<cmd>lua require"dap".repl.open()<CR>', { noremap = true })
map("n", "<leader>drl", '<cmd>lua require"dap".repl.run_last()<CR>', { noremap = true })

-- dap-ui
-- map('n', '<leader>dsc', '<cmd>lua require"dapui".scopes()<CR>', { noremap = true })
-- map('n', '<leader>dhh', '<cmd>lua require"dapui".hover()<CR>', { noremap = true })
-- map('v', '<leader>dhv', '<cmd>lua require"dapui".visual_hover()<CR>', { noremap = true })
-- map('n', '<leader>duh', '<cmd>lua require"dapui".hover()<CR>', { noremap = true })
-- map('n', '<leader>duf', "<cmd>lua local widgets=require'dap.ui.widgets';widgets.centered_float(widgets.scopes)<CR>", { noremap = true })
map("n", "<leader>dui", '<cmd>lua require"dapui".toggle()<CR>', { noremap = true })

-- telescope-dap
map("n", "<leader>dcc", '<cmd>lua require"telescope".extensions.dap.commands{}<CR>', { noremap = true })
map("n", "<leader>dco", '<cmd>lua require"telescope".extensions.dap.configurations{}<CR>', { noremap = true })
map("n", "<leader>dlb", '<cmd>lua require"telescope".extensions.dap.list_breakpoints{}<CR>', { noremap = true })
map("n", "<leader>dv", '<cmd>lua require"telescope".extensions.dap.variables{}<CR>', { noremap = true })
map("n", "<leader>df", '<cmd>lua require"telescope".extensions.dap.frames{}<CR>', { noremap = true })
