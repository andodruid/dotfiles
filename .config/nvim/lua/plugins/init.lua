vim.cmd("packadd packer.nvim")

require("packer").startup(function(use)
	-- package manager
	use({ "wbthomason/packer.nvim" })

	-- icons for some plugins
	use({
		"kyazdani42/nvim-web-devicons",
		config = function()
			require("plugins/devicons")
		end,
	})

	use({ "ThePrimeagen/vim-be-good" })

	-- themes
	use({
		"folke/tokyonight.nvim",
		config = function()
			require("plugins/theme")
		end,
	})
	use({
		"kabouzeid/nvim-jellybeans",
		requires = { "rktjmp/lush.nvim" },
		config = function()
			require("plugins/theme")
		end,
	})

	use({
		"gbprod/nord.nvim",
		config = function()
			require("plugins/theme")
		end,
	})

	use({
		"catppuccin/nvim",
		config = function()
			require("plugins/theme")
		end,
	})
	use({
		"tjdevries/colorbuddy.nvim",
		config = function()
			require("plugins/theme")
		end,
	})

	-- indent-blankline
	use({
		"lukas-reineke/indent-blankline.nvim",
		config = function()
			require("plugins/indent")
		end,
	})

	-- nvimtree
	use({
		"nvim-tree/nvim-tree.lua",
		config = function()
			require("plugins/nvimtree")
		end,
	})

	-- floaterm
	use({
		"voldikss/vim-floaterm",
		config = function()
			require("plugins/floaterm")
		end,
	})

	use({
		"simrat39/rust-tools.nvim",
		config = function()
			require("plugins/rusttools")
		end,
	})

	use({
		"simrat39/inlay-hints.nvim",
		config = function()
			require("plugins/inlayhints")
		end,
	})

	-- language server protocol
	use({
		"neovim/nvim-lspconfig",
		config = function()
			require("lsp")
		end,
	})

	use({
		"williamboman/mason.nvim",
		config = function()
			require("lsp/mason")
		end,
	})

	-- lsp formatting with null-ls
	use({
		"jose-elias-alvarez/null-ls.nvim",
		config = function()
			require("lsp/null-ls")
		end,
	})

	-- treesitter
	use({
		"nvim-treesitter/nvim-treesitter",
		requires = {
			run = function()
				vim.cmd("TSUpdate")
			end,
		},
		config = function()
			require("plugins/treesitter")
		end,
	})

	use({
		"nvim-treesitter/nvim-treesitter-context",
		config = function()
			require("plugins/treesitter-context")
		end,
	})

	-- harpoon
	use({
		"ThePrimeagen/harpoon",
		config = function()
			require("plugins/harpoon")
		end,
	})

	-- debug adapter protocol (nvim-dap)
	use({ "mfussenegger/nvim-dap" })
	use({ "nvim-telescope/telescope-dap.nvim" })
	use({ "mfussenegger/nvim-dap-python" })
	use({ "Pocco81/DAPInstall.nvim" })
	use({ "rcarriga/nvim-dap-ui" })

	-- telescope
	use({
		"nvim-telescope/telescope.nvim",
		requires = { { "nvim-lua/plenary.nvim" } },
		config = function()
			require("plugins/telescope")
		end,
	})

	-- completion
	use({
		"hrsh7th/nvim-cmp",
		config = function()
			require("plugins/completion")
		end,
	})
	use({ "hrsh7th/cmp-nvim-lsp" })
	use({ "onsails/lspkind.nvim" })
	use({ "hrsh7th/cmp-buffer" })
	use({ "hrsh7th/cmp-path" })
	use({ "hrsh7th/cmp-cmdline" })
	-- ultisnips works with cmp for snippets
	use({ "SirVer/ultisnips" })
	use({ "quangnguyen30192/cmp-nvim-ultisnips" })
	use({ "honza/vim-snippets" })

	-- statusline
	use({
		"nvim-lualine/lualine.nvim",
		config = function()
			require("plugins/lualine")
		end,
	})

	use({
		"lewis6991/gitsigns.nvim",
		requires = { "nvim-lua/plenary.nvim" },
		config = function()
			require("plugins/gitsigns")
		end,
	})

	-- various
	use({
		"windwp/nvim-autopairs",
		config = function()
			require("plugins/autopairs")
		end,
	})
	use({
		"alvan/vim-closetag",
		config = function()
			require("plugins/closetag")
		end,
	})
	use({ "tpope/vim-surround" })
	use({ "tpope/vim-commentary" })
	use({
		"vim-test/vim-test",
		config = function()
			require("plugins/vimtest")
		end,
	})

	-- formatter
	-- use({
	-- 	"mhartington/formatter.nvim",
	-- 	config = function()
	-- 		require("plugins/formatter")
	-- 	end,
	-- })

	use({
		"norcalli/nvim-colorizer.lua",
		config = function()
			require("plugins/colorizer")
		end,
	})

	-- bufferline
	use({
		"akinsho/bufferline.nvim",
		requires = "kyazdani42/nvim-web-devicons",
		config = function()
			require("plugins/bufferline")
		end,
	})
end)
