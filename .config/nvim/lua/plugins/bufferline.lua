local bufferline = require("bufferline")

if not bufferline then
	return
end

bufferline.setup({
	options = {
		numbers = "ordinal",
		-- indicator = { style = "underline" },
		show_buffer_close_icons = false,
		show_close_icon = false,
		show_buffer_icons = false,
		diagnostics = "nvim_lsp",
		show_buffer_default_icon = false,
		offsets = { { filetype = "NvimTree", text = "", padding = 1, separator = false } },
		max_name_length = 16,
	},
})
