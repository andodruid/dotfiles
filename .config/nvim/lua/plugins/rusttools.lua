local rt = require("rust-tools")
local on_attach = require("../lsp/on_attach")
local capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())

rt.setup({
	server = {
		on_attach = function(c)
			on_attach(c)
		end,
		capabilities = capabilities,
		settings = {
			["rust-analyzer"] = {
				-- checkOnSave = { command = "clippy",
				-- },
			},
		},
	},
	tools = {
		inlay_hints = {
			auto = true,
			show_parameter_hints = true,
		},
	},
})
