local colors = {
	blue = "#009CFF",
	yellow = "#F5DF4E",
	cherry = "#b16286",
	black = "#181825",
	white = "#c6c6c6",
	red = "#E3583E",
	green = "#009572",
	flamingo = "#F7CAC9",
	grey = "#1e1e2e",
	teal = "#94e2d5",
}

local groovy = {
	normal = {
		a = { fg = colors.black, bg = colors.green },
		b = { fg = colors.white, bg = nil },
		c = { fg = colors.black, bg = nil },
	},

	insert = { a = { fg = colors.black, bg = colors.blue } },
	visual = { a = { fg = colors.black, bg = colors.yellow } },
	replace = { a = { fg = colors.black, bg = colors.red } },

	inactive = {
		a = { fg = colors.white, bg = colors.black },
		b = { fg = colors.white, bg = colors.black },
		c = { fg = colors.black, bg = colors.black },
	},
}

local conditions = {
	not_empty = function()
		return vim.fn.empty(vim.fn.expand("%:t")) ~= 1
	end,
	hide_width = function()
		return vim.fn.winwidth(0) > 80
	end,
	check_git_workspace = function()
		local filepath = vim.fn.expand("%:p:h")
		local gitdir = vim.fn.finddir(".git", filepath .. ";")
		return gitdir and #gitdir > 0 and #gitdir < #filepath
	end,
}

require("lualine").setup({
	options = {
		theme = groovy,
		component_separators = "|",
		section_separators = { left = "", right = "" },
	},
	sections = {
		lualine_a = {
			{ "mode", color = { gui = "bold" }, separator = { right = "" }, right_padding = 2 },
		},
		lualine_b = {
			{
				"diagnostics",
				sources = { "nvim_diagnostic" },
				symbols = { error = " ", warn = " ", info = " " },
				diagnostics_color = {
					color_error = { fg = colors.red },
					color_warn = { fg = colors.yellow },
					color_info = { fg = colors.blue },
				},
			},
			{
				"filename",
				path = 1,
				file_status = true,
				color = { fg = colors.teal, gui = "bold" },
				separator = { right = "" },
				cond = conditions.not_empty,
			},
			{
				"branch",
				color = { fg = colors.flamingo, gui = "bold" },
				separator = { right = "" },
			},
			{
				"diff",
				symbols = { added = " ", modified = "柳", removed = " " },
				separator = { right = "", left = "" },
				diff_color = {
					added = { fg = colors.green },
					modified = { fg = colors.yellow },
					removed = { fg = colors.red },
				},
			},
		},
		lualine_c = {},
		lualine_x = {},
		lualine_y = {
			{
				"filesize",
				color = { gui = "bold", fg = colors.flamingo },
				cond = conditions.not_empty,
			},
			{
				"filetype",
				color = { gui = "bold", fg = colors.teal },
			},
			{
				"fileformat",
				symbols = {
					unix = "UNIX", -- e712
					dos = "", -- e70f
					mac = "", -- e711
				},
				color = { gui = "bold", fg = colors.teal },
			},
			{ "progress", color = { gui = "bold", fg = colors.teal } },
		},
		lualine_z = {
			{
				"location",
				separator = { left = "" },
				left_padding = 2,
				color = { gui = "bold", bg = colors.teal },
			},
		},
	},
	inactive_sections = {
		lualine_a = { "filename" },
		lualine_b = {},
		lualine_c = {},
		lualine_x = {},
		lualine_y = {},
		lualine_z = { "location" },
	},
	tabline = {},
	extensions = {},
})
