local treesitter = require("nvim-treesitter.configs")
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
vim.opt.fillchars = "fold: "
vim.opt.foldlevel = 99

treesitter.setup({
	ensure_installed = {
		"tsx",
		"prisma",
		"bash",
		"toml",
		"html",
		"javascript",
		"typescript",
		"go",
		"scss",
		"css",
		"json",
		"lua",
	}, -- one of "all", "maintained" (parsers with maintainers), or a list of languages
	highlight = {
		enable = true,
	},
	indent = {
		enable = false,
	},
	incremental_selection = {
		enable = false,
	},
	textobjects = {
		enable = true,
	},
	rainbow = {
		enable = true,
		extended_mode = true,
	},
})
