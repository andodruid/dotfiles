local map = vim.api.nvim_set_keymap

map("n", "<Leader>hh", '<cmd>lua require"harpoon.ui".toggle_quick_menu()<CR>', { noremap = true }) -- access last open buffer
map("n", "<Leader>ha", '<cmd>lua require"harpoon.mark".add_file()<CR>', { noremap = true }) -- access last open buffer

map("n", "<Leader>1", '<cmd>lua require"harpoon.ui".nav_file(1)<CR>', { noremap = true }) -- access last open buffer
map("n", "<Leader>2", '<cmd>lua require"harpoon.ui".nav_file(2)<CR>', { noremap = true }) -- access last open buffer
map("n", "<Leader>3", '<cmd>lua require"harpoon.ui".nav_file(3)<CR>', { noremap = true }) -- access last open buffer
map("n", "<Leader>4", '<cmd>lua require"harpoon.ui".nav_file(4)<CR>', { noremap = true }) -- access last open buffer
map("n", "<Leader>5", '<cmd>lua require"harpoon.ui".nav_file(5)<CR>', { noremap = true }) -- access last open buffer
