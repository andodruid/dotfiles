##### PATH #####
set -g -x PATH /usr/bin /usr/local/bin $HOME/.local/bin $HOME/go/bin /opt/homebrew/bin/ $HOME/.cargo/bin $PATH

if type -q nvm
    nvm use lts > /dev/null
else
    echo 'nvm is not installed'
end

##### SETUP SCRIPT #####
function fish-setup
    git config --global core.editor 'nvim'
    curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
    fisher install jorgebucaran/nvm.fish
    fisher install jethrokuan/z
    nvm install lts
    nvm use lts
    npm install -g prettier \
      eslint typescript typescript-language-server \
      vscode-langservers-extracted vue-language-server
end

#### KILL TMUX PANE ####
function x 
    set tpid (tmux run "echo '#{pane_id}'")
    tmux kill-pane -t $tpid
end

##### UPDATE BG AND KITTY BGIMG SIMULTANEOUSLY #####
function update-bg
    feh --bg-scale $argv[1]
    echo "background_image $argv[1]" > "$HOME/.config/kitty/background_image.conf"
end

function sync
    $argv[1] add -u
    $argv[1] commit -m "auto sync commit"
    if test $argv[1] = "md"
        echo "pushing to bare repo: $argv[1]"
        $argv[1] push git@gitlab.com:andodruid/markdown.git
    else if test $argv[1] = "dot"
        echo "pushing to bare repo: $argv[1]"
        $argv[1] push git@gitlab.com:andodruid/dotfiles.git
    else 
        echo "Synchronization with that bare repo does not exist."
    end
end

##### ALIASES ####
source ~/.config/fish/shell_aliases
